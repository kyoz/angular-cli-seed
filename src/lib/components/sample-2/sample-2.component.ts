import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sample-2-component',
  templateUrl: './sample-2.component.html',
  styleUrls: ['./sample-2.component.scss']
})
export class Sample2Component implements OnInit {

  constructor(private _router: Router) { }

  ngOnInit() {
  }

  goNext() {
    this._router.navigate(['/seed/sample']);
  }
}
