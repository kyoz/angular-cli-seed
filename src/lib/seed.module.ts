import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import 'rxjs/add/observable/fromEvent';

/* Angular dependencies */
import {
  MatToolbarModule,
  MatButtonModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

/* User dependencies */
import { SeedRoutingModule } from './seed-routing.module';

import {
  MainComponent,
  SampleComponent,
  Sample2Component,
} from './components';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    SeedRoutingModule,
    MatToolbarModule,
    MatButtonModule

  ],
  declarations: [
    MainComponent,
    SampleComponent,
    Sample2Component,
  ],
  providers: []
})
export class SeedModule { }
