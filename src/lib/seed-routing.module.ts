import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
  MainComponent,
  SampleComponent,
  Sample2Component
} from './components';


const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        children: [
          { path: 'sample', component: SampleComponent },
          { path: 'sample2', component: Sample2Component }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class SeedRoutingModule { }
